<?php
/**
 * ============================================================================
 * 版权所有 2017-2077 tpframe工作室，并保留所有权利。
 * @link http://www.tpframe.com/
 * @copyright Copyright (c) 2017 TPFrame Software LLC
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下对程序代码进行修改和使用；
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 */
namespace app\frontend\controller;
use \tpfcore\Core;
use app\frontend\model\SlideCat;
use app\frontend\model\Slide;
use think\Cookie;
class Index extends FrontendBase
{
    public function index()
    {
        $listIndexProduct = Core::loadModel("Posts")->getTpfPosts([
            "where"=>["cateid"=>["in",["1","2","3","4","5"]],"isdelete"=>0,"ischeck"=>1],
            "field"=>"id,title,thumb",
            "limit"=>8
        ]);
        $listBzProduct = Core::loadModel("Posts")->getTpfPosts([
            "where"=>["cateid"=>2,"isdelete"=>0,"ischeck"=>1],
            "field"=>"id,title,thumb",
            "limit"=>4
        ]);
        $listIndexNews = Core::loadModel("Posts")->getTpfPosts([
            "where"=>["cateid"=>7,"isdelete"=>0,"ischeck"=>1],
            "field"=>"id,title,content,datetime",
            "limit"=>6
        ]);
    	return $this->fetch("index",[
            "listIndexProduct"=>$listIndexProduct,
            "listBzProduct"=>$listBzProduct,
            "listIndexNews"=>$listIndexNews
        ]);
    }
    public function product(){
        $where = isset($this->param['cid'])?["cateid"=>$this->param['cid']]:["cateid"=>["in",["1","2","3","4","5"]]];
        $where['isdelete']=0;
        $where['ischeck']=1;

        $list = Core::loadModel("Posts")->getTpfPosts([
            "where"=>$where,
            "field"=>"id,title,thumb,parentid",
            "paginate"  =>['rows' =>12],
        ]);
        return $this->fetch("product",["list"=>$list]);
    }
    public function product_detail(){
         $list = Core::loadModel("Posts")->getTpfPosts([
            "where"=>["id"=>$this->param['id'],"isdelete"=>0,"ischeck"=>1],
            "field"=>"id,title,content,cateid,datetime"
        ]);
        return $this->fetch("product_detail",['list'=>$list]);
    }
    public function news(){
        $where = isset($this->param['cid'])?["cateid"=>$this->param['cid']]:["cateid"=>7];
        $where['isdelete']=0;
        $where['ischeck']=1;

        $list = Core::loadModel("Posts")->getTpfPosts([
            "where"=>$where,
            "field"=>"id,title,thumb,content,datetime",
            "paginate"  =>['rows' =>12],
        ]);
        return $this->fetch("news",["list"=>$list]);
    }
    public function news_detail(){
        $list = Core::loadModel("Posts")->getTpfPosts([
            "where"=>["id"=>$this->param['id'],"isdelete"=>0,"ischeck"=>1],
            "field"=>"id,title,content,parentid,cateid,datetime"
        ]);
        return $this->fetch("news_detail",['list'=>$list]);
    }
    public function contact(){
        return $this->fetch("contact");   
    }
    public function cases()
    {
    	return $this->fetch("cases",[
            "list"=>Core::loadModel("Posts")->listPosts($this->param,3)
        ]);
    }
    public function about()
    {
    	return $this->fetch("about");
    }
    public function changlang(){
       Cookie::set("think_var",$this->param['lang']);
    }
}
