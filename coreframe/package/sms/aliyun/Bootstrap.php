<?php
namespace sms\aliyun;

Bootstrap::addAutoloadPath();

spl_autoload_register(["\sms\aliyun\Bootstrap","autoload"]);

class Bootstrap
{
    private static $autoloadPathArray = array(
        "",
        "aliyun",
        "aliyun\Auth",
        "aliyun\Http",
        "aliyun\Profile",
        "aliyun\Regions",
        "aliyun\Exception",
        "aliyun\Api\Sms\Request\V20170525"
    );
    
    public static function autoload($className)
    {
        foreach (self::$autoloadPathArray as $path) {

        	$file = dirname(__DIR__).DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$className.".php";

            $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);

            if (is_file($file)) {

                include_once $file;

                break;

            }
        }
    }
    
    public static function addAutoloadPath($path="")
    {
    	if(!empty($path) && is_array($path)){

    		self::$autoloadPathArray=array_keys(array_flip(self::$autoloadPathArray)+array_flip($path));

    	}
    	if(!empty($path) && is_string($path) && !in_array($path,self::$autoloadPathArray)){

    		array_push(self::$autoloadPathArray, $path);
    		
    	}
    }
}
